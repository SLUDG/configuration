name := "config"
organization := "com.sludg.util"
version := "0.1"

scalaVersion := "2.13.0"
crossScalaVersions := Seq("2.12.10", "2.11.12")

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.0.8" % Test,
  "com.typesafe" % "config" % "1.3.4"
)

enablePlugins(GitlabPlugin)
com.sludg.sbt.gitlab.GitlabPlugin.gitlabProjectId := "14257802"