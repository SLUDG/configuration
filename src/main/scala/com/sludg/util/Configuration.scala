package com.sludg.util

import com.typesafe.config.{ Config, ConfigFactory }
import scala.util.Try

object Configuration {

  /**
   * An implicit class to add custom methods to a Config
   * @param config A Typesafe Config
   */
  implicit class EnhancedConfig(config: Config) {

    /**
     * A nullary function to wrap a Config action in a Try[A]
     * @param tryAction
     * @tparam A
     * @return
     */
    def tryAction[A](tryAction: Config => A): Try[A] = Try {
      tryAction(config)
    }

    /**
     * A nullary function to wrap a Config action in a Try[Option[A]] and then indiscriminately flatten to an Option[A]
     * @param optAction
     * @tparam A
     * @return
     */
    def optAction[A](optAction: Config => A): Option[A] = Try {
      Option {
        optAction(config)
      }
    }.toOption.flatten

    /**
     * Get the config path as an Option[String]
     * @param path
     * @return
     */
    def getStringOpt(path: String): Option[String] = optAction(_.getString(path))

    /**
     * Get the config path as an Option[Int]
     * @param path
     * @return
     */
    def getIntOpt(path: String): Option[Int] = optAction(_.getInt(path))

    /**
     * Get the config path as an Option[Boolean]
     * @param path
     * @return
     */
    def getBooleanOpt(path: String): Option[Boolean] = optAction(_.getBoolean(path))

  }

  /**
   * A lazy-loaded Config
   */
  lazy val config: Config = ConfigFactory.load()

}

