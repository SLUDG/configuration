package com.sludg.util

import com.typesafe.config.Config
import org.scalatest.{FlatSpec, Matchers}
import Configuration.EnhancedConfig

import scala.util.Try

class ConfigurationSpec extends FlatSpec with Matchers {

  val configScope: String = "com.sludg.util.config"
  val testConfig: Config = Configuration.config
  val scopedConfig: Config = testConfig.getConfig(configScope)

  "Configuration" should "be able to load the reference config" in {
    assert(!scopedConfig.isEmpty)
  }

  it should "correctly resolve a Try action for a path that is/isn't present" in {

    val pathSuccess: Try[AnyRef] = scopedConfig.tryAction(_.getAnyRef("stringVal"))
    val pathFailure = scopedConfig.tryAction(_.getAnyRef("notPresent"))

    assert(pathSuccess.isSuccess)
    assert(pathFailure.isFailure)

  }

  it should "correctly resolve a Option action for a path that is/isn't present" in {

    val pathSuccess: Option[AnyRef] = scopedConfig.optAction(_.getAnyRef("stringVal"))
    val pathFailure: Option[AnyRef] = scopedConfig.optAction(_.getAnyRef("notPresent"))

    assert(pathSuccess.nonEmpty)
    assert(pathFailure.isEmpty)

  }

  it should "correctly resolve the Typed Option get methods" in {

    val stringSuccess: Option[String] = scopedConfig.getStringOpt("stringVal")
    assert(stringSuccess.exists(s => s.isInstanceOf[String]))

    val intSuccess: Option[Int] = scopedConfig.getIntOpt("intVal")
    assert(intSuccess.exists(i => i.isInstanceOf[Int]))

    val boolSuccess: Option[Boolean] = scopedConfig.getBooleanOpt("boolVal")
    assert(boolSuccess.exists(i => i.isInstanceOf[Boolean]))

  }


}
